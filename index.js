arr = [1, 2, 3, 4, 5];

const modifyEverySecondElement = (arr) => {
  const modifiedArr = arr.map((ele, index) =>
    index % 2 !== 0 ? ele + 1 : ele
  );
  return modifiedArr;
};

console.log(modifyEverySecondElement(arr));
